# Python Settings
# pip should only run if there is a virtualenv currentyl activated
export PIP_REQUIRE_VIRTUALENV=true

# disable virtualenv for pip installing a global package
# gpip install package
gpip(){
   PIP_REQUIRE_VIRTUALENV="" pip "$@"
}

gpip3(){
   PIP_REQUIRE_VIRTUALENV="" pip3 "$@"
}
